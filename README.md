# Drupal 8 Scoreboard Module #

This is a module I threw together to play with Drupal 8.

### There are branches for each step I went through ###

* Step 1: Basic page
* Step 2: Add twig
* Step 3: Get JSON for scoreboard
* Step 4: Make JSON fetcher a service
* Step 5: Use twig
* Step 6: Caching JSON
* Step 7: Add CSS using libraries