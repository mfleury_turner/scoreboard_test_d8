<?php
namespace Drupal\scoreboard;

class JsonFetcher {
    public function fetch($url) {
        $data = file_get_contents($url);
        $data = json_decode($data);
        return $data;
    }
}
