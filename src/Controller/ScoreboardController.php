<?php
/**
 * @file
 * Contains \Drupal\scoreboard\Controller\ScoreboardController.
 */

namespace Drupal\scoreboard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\scoreboard\JsonFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for scoreboard routes.
 */
class ScoreboardController extends ControllerBase {
    protected $jsonFetcher;

    /**
     * @param \Drupal\scoreboard\JsonFetcher $json_fetcher
     */
    public function __construct(Jsonfetcher $json_fetcher) {
        $this->jsonFetcher = $json_fetcher;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('scoreboard.json_fetcher')
        );
    }

    /**
     * Builds the app page.
     *
     * @return array
     *   Array of page elements to render.
     */
    public function page() {
        $games = $this->jsonFetcher->fetch('http://i.turner.ncaa.com/sites/default/files/external/test/scoreboard.json');
kpr($games);
        $output['scoreboard'] = array(
            '#theme' => 'scoreboard',
            '#games' => $games,
            '#date'  => 'today',
        );
        return $output;
    }
}
